# Computer Organization 2 Project 1

# Team Member Names
David Awogbemila, Junior

Temilola Oloyede, Junior

# Explanation of how to run program
1. Load program into QTSpim and run
2. Enter first integer operand at prompt and press enter
3. Enter operator(+, -, *, /) at prompt. DO NOT PRESS ENTER. (MIPS expects a single character)
4. Enter second integer operand at prompt and press enter
5. The program prints operand1 *OPERATOR* operand2 = result. 

# Run Tests
1. Reinitialize and Load domath_test.S
2. Load domath.S
3. Accept any errors that pop up
4. Click Run.

# Project Description
The workload was split as:

* Main - Temilola
* Do_math - David
* Do_add - Temilola
* Do_subtract - David
* Do_multiply - David
* Do_divide - Temilola

# Test Cases

## Divide Test Cases

* Test Case 1:

	Testing that divide works correctly for quotient and remainder values
	
	$a0(Dividend): 156   
	
	$a2(Divisor): 156
	
	Expected Output: $v0 (Quotient) = 1, $v1 (Remainder) = 0
	
	Actual Output: $v0 (Quotient) = 1, $v1 (Remainder) = 0
	
* Test Case 2:
	
	Testing that divide works correctly for quotient and remainder values
	
	$a0(Dividend): -156   
	
	$a2(Divisor): 156
	
	Expected Output: $v0 (Quotient) = -1, $v1 (Remainder) = 0
	
	Actual Output: $v0 (Quotient) = -1, $v1 (Remainder) = 0
	
* Test Case 3:
	
	Testing that divide works correctly for quotient and remainder values
	
	$a0(Dividend): 12  
	
	$a2(Divisor): 14
	
	Expected Output: $v0 (Quotient) = 0, $v1 (Remainder) = 12
	
	Actual Output: $v0 (Quotient) = 0, $v1 (Remainder) = 12
	
* Test Case 4:
	
	Testing that divide works correctly for quotient and remainder values
	
	$a0(Dividend): 11   
	
	$a2(Divisor): 3
	
	Expected Output: $v0 (Quotient) = 3, $v1 (Remainder) = 2
	
	Actual Output: $v0 (Quotient) = 3, $v1 (Remainder) = 2
	
* Test Case 5:
	
	Testing that divide works correctly for quotient and remainder values
	
	$a0(Dividend): 0 
	
	$a2(Divisor): 1
	
	Expected Output: $v0 (Quotient) = 0, $v1 (Remainder) = 0
	
	Actual Output: $v0 (Quotient) = 0, $v1 (Remainder) = 0
	
* Test Case 6:
	
	Testing that divide works correctly for quotient and remainder values and undefined value
	
	$a0(Dividend): 0   
	
	$a2(Divisor): 0
	
	Expected Output: $s1 (undefined) = 1
	
	Actual Output: $s1 (undefined) = 1
	
* Test Case 7:

	Testing that divide works correctly for quotient and remainder values and undefined value
	
	$a0(Dividend): 11754   
	
	$a2(Divisor): 0
	
	Expected Output: $s1 (undefined) = 1
	
	Actual Output: $s1 (undefined) = 1
	
	
## Add Test Cases

* Test Case 1:

	Testing that add works correctly
	
	$a0 (First Operand): 147556
	
	$a2 (Second Operand): 0
	
	Expected Output: $v0 (Sum) = 147556, $v1 (overflow) = 0
	
	Actual Output : $v0 (Sum) = 147556, $v1 (overflow) = 0
	
* Test Case 2:
	
	Testing that add works correctly
	
	$a0 (First Operand): 147556
	
	$a2 (Second Operand): -14678
	
	Expected Output: $v0 (Sum) = 132878, $v1 (overflow) = 0
	
	Actual Output : $v0 (Sum) = 132878, $v1 (overflow) = 0
	
* Test Case 3:

	Testing that add works correctly
	
	$a0 (First Operand): 2147483646
	
	$a2 (Second Operand): 2147483646
	
	Expected Output: $v0 (Sum) = , $v1 (overflow) = 1
	
	Actual Output : $v0 (Sum) = , $v1 (overflow) = 1

* Test Case 4:

	Testing that add works correctly
	
	$a0 (First Operand): 0
	
	$a2 (Second Operand): 0
	
	Expected Output: $v0 (Sum) = 0, $v1 (overflow) = 0
	
	Actual Output : $v0 (Sum) = 0, $v1 (overflow) = 0

## Multiplication Test Cases

* Test Case 1:

	Testing that multiply works correctly
	
	$a0 (First Operand): 2
	
	$a2 (Second Operand): 3
	
	Expected Output: $v0 (Multiply) = 6
	
	Actual Output : $v0 (Multiply) = 6
	
* Test Case 2:
	
	Testing that multiply works correctly
	
	$a0 (First Operand): -2
	
	$a2 (Second Operand): 3
	
	Expected Output: $v0 (Multiply) = -6
	
	Actual Output : $v0 (Multiply) = -6
	
* Test Case 3:

	Testing that multiply works correctly
	
	$a0 (First Operand): -2
	
	$a2 (Second Operand): -3
	
	Expected Output: $v0 (Multiply) = 6
	
	Actual Output : $v0 (Multiply) = 6

* Test Case 4:

	Testing that multiply works correctly
	
	$a0 (First Operand): 600000
	
	$a2 (Second Operand): 600000
	
	Expected Output: $v0 (Multiply) = 83, $v1 (HIGH) = -777252864
	
	Actual Output : $v0 (Multiply) = 83, $v1 (HIGH) = -777252864

	(NOTE: Actual $v1 output is -777252864 which we determined to be correct based on the
	default mult function, but the test case kept returning false; we suspect this is because it is beyond the 16-bti range for immediate values.)

* Test Case 5:

	Testing that multiply works correctly
	
	$a0 (First Operand): 2147483647
	
	$a2 (Second Operand): 2
	
	Expected Output: $v0 (Multiply) = -2,  $v1 = 0
	
	Actual Output : $v0 (Multiply) = -2,  $v1 = 0

## Subtract Test Cases
	
	Note: do_subtract implicitly returns the overflow signal by not altering the value of $v1 where do_add records overflow because do_subtract
	simply uses do_add.
* Test Case 1:

	Testing that subtract works correctly
	
	$a0 (First Operand): 2
	
	$a2 (Second Operand): 3
	
	Expected Output: $v0 = -1
	
	Actual Output : $v0 ) = -1
	
* Test Case 2:
	
	Testing that subtract works correctly
	
	$a0 (First Operand): -2
	
	$a2 (Second Operand): 3
	
	Expected Output: $v0 = -5
	
	Actual Output : $v0 = -5
	
* Test Case 3:

	Testing that subtract works correctly
	
	$a0 (First Operand): -2
	
	$a2 (Second Operand): -3
	
	Expected Output: $v0 = 1
	
	Actual Output : $v0 = 1

* Test Case 4:

	Testing that subtract works correctly
	
	$a0 (First Operand): 600000
	
	$a2 (Second Operand): 600000
	
	Expected Output: $v0 = 0
	
	Actual Output : $v0 = 0


* Test Case 5:

	Testing that subtract works correctly
	
	$a0 (First Operand): 0
	
	$a2 (Second Operand): -1
	
	Expected Output: $v0 = 1
	
	Actual Output : $v0 = 1

